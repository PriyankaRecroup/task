class HomeController < ApplicationController
  def index
     unless current_user.present?
       redirect_to '/users/sign_in'
     end
  end
end