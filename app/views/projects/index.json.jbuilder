json.array!(@projects) do |project|
  json.extract! project, :id, :title, :income, :expense, :client_id, :project_desc
  json.url project_url(project, format: :json)
end
