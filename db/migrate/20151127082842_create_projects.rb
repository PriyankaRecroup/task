class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.integer :income
      t.integer :expense
      t.integer :client_id
      t.text :project_desc

      t.timestamps
    end
  end
end
